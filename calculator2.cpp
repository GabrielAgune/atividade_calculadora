#include "calculator2.h"
#include "ui_calculator2.h"

double primeiroNum;
bool segundoNumDigitado;


Calculator2::Calculator2(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Calculator2)
{
    ui->setupUi(this);

    //Números
    connect(ui->Button0, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button1, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button2, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button3, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button4, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button5, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button6, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button7, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button8, SIGNAL(released()),this, SLOT(NumPressed()));
    connect(ui->Button9, SIGNAL(released()),this, SLOT(NumPressed()));


    //Ponto, igual, porcentagem, Clear e delete
    connect(ui->ButtonDot, SIGNAL(released()),this, SLOT(Decimal()));
    connect(ui->ButtonEq, SIGNAL(released()),this, SLOT(Equals()));
    connect(ui->ButtonAC, SIGNAL(released()),this, SLOT(Clear()));
    connect(ui->ButtonPercent, SIGNAL(released()),this, SLOT(Percent()));
    connect(ui->ButtonDEL, SIGNAL(released()),this, SLOT(Delete()));


    //Operações
    connect(ui->ButtonAdd, SIGNAL(released()),this, SLOT(Operations()));
    connect(ui->ButtonSub, SIGNAL(released()),this, SLOT(Operations()));
    connect(ui->ButtonDiv, SIGNAL(released()),this, SLOT(Operations()));
    connect(ui->ButtonMult, SIGNAL(released()),this, SLOT(Operations()));

    //tornando os botões de operação "Checkaveis"
    ui->ButtonAdd->setCheckable(true);
    ui->ButtonSub->setCheckable(true);
    ui->ButtonDiv->setCheckable(true);
    ui->ButtonMult->setCheckable(true);
}

Calculator2::~Calculator2()
{
    delete ui;
}

//Função Botão dos Números
void Calculator2::NumPressed(){
    QPushButton *button = (QPushButton*)sender();
    double displayNumber;
    QString newDisplay;

    // se alguma operação está checkada -> insere o segundo número
    if((ui->ButtonAdd->isChecked() || ui->ButtonSub->isChecked() ||
            ui->ButtonDiv->isChecked() || ui->ButtonMult->isChecked()) && (!segundoNumDigitado)){
        displayNumber = button->text().toDouble();
        segundoNumDigitado = true;
        newDisplay = QString::number(displayNumber,'g', 15);
    }
    //senão há nenhuma operação checkada continua a compor o número
    else {
        //para adicionar 0 após a vírgula
        if (ui->Display->text().contains(".") && button->text() == "0"){
            newDisplay = (ui->Display->text() + button->text());
        }
        //função inicial
        else {
        displayNumber = (ui->Display->text() + button->text()).toDouble();
        newDisplay = QString::number(displayNumber,'g', 15);
        }
    }
    ui->Display->setText(newDisplay);
}

//Função de porcentagem
void Calculator2::Percent(){
    QPushButton *button = (QPushButton*)sender();
    double displayNumber;
    QString newDisplay;

    if (button->text() == "%"){
        displayNumber = ui->Display->text().toDouble();
        displayNumber = displayNumber * 0.01;
        newDisplay = QString::number(displayNumber,'g', 15);
        ui->Display->setText(newDisplay);
    }
}

//Função da vírgula
void Calculator2::Decimal(){
    ui->Display->setText(ui->Display->text() + ".");
}

//Função clear
void Calculator2::Clear(){
    //reseta todas as variáveis e o display
    ui->ButtonAdd->setChecked(false);
    ui->ButtonSub->setChecked(false);
    ui->ButtonDiv->setChecked(false);
    ui->ButtonMult->setChecked(false);

    segundoNumDigitado = false;
    ui->Display->setText("0");
}

//Função do igual
void Calculator2::Equals(){
    double displayNumber, segundoNum;
    QString newDisplay;

    segundoNum = ui->Display->text().toDouble();

    if (ui->ButtonAdd->isChecked())
    {
        displayNumber = primeiroNum + segundoNum;
        newDisplay = QString::number(displayNumber,'g',15);
        ui->Display->setText(newDisplay);
        ui->ButtonAdd->setChecked(false);
    }
    else if (ui->ButtonSub->isChecked())
    {
        displayNumber = primeiroNum - segundoNum;
        newDisplay = QString::number(displayNumber,'g',15);
        ui->Display->setText(newDisplay);
        ui->ButtonSub->setChecked(false);
    }
    else if (ui->ButtonDiv->isChecked())
    {
        displayNumber = primeiroNum / segundoNum;
        newDisplay = QString::number(displayNumber,'g',15);
        ui->Display->setText(newDisplay);
        ui->ButtonDiv->setChecked(false);
    }
    else if (ui->ButtonMult->isChecked())
    {
        displayNumber = primeiroNum * segundoNum;
        newDisplay = QString::number(displayNumber,'g',15);
        ui->Display->setText(newDisplay);
        ui->ButtonMult->setChecked(false);
    }

    segundoNumDigitado = false;
}

//Função das operações
void Calculator2::Operations(){

    //define o primeiro número para realizar as operações
    QPushButton *button = (QPushButton*)sender();
    primeiroNum = ui->Display->text().toDouble();
    button->setChecked(true);

}

void Calculator2::Delete(){
    QString deleta = ui->Display->text();
    deleta.chop(1);
    if (deleta.isEmpty()){
        deleta = "0";
    }
    ui->Display->setText(deleta);
}
