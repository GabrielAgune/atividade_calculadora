#ifndef CALCULATOR2_H
#define CALCULATOR2_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Calculator2; }
QT_END_NAMESPACE

class Calculator2 : public QMainWindow
{
    Q_OBJECT

public:
    Calculator2(QWidget *parent = nullptr);
    ~Calculator2();

private:
    Ui::Calculator2 *ui;

private slots:
    void NumPressed();
    void Percent();
    void Decimal();
    void Clear();
    void Equals();
    void Operations();
    void Delete();


};
#endif // CALCULATOR2_H
